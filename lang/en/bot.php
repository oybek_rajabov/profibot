<?php

namespace DefStudio\Telegraph\Lang;

return [
    'wrong format' => "Wrong format!",
    'change language' => "Change language",
    'send phone' => "Send number",
    'send your phone in format' => "Send your phone number in the form +998 ** *** ****, \n or click the «📱 Send number» button:",
    'send verification code' => "❗❓ Please type verification code which have been sent to your phone number.",
    'the phone is already exist' => '❌ The phone number is already exist!',
    'code is not correct' => '❌ Incorrect!',
    'send your private informations' => "❗Type your private informations.",
    'log out' => 'Log out',
    'fio' => "SNP",
    'passport seria' => 'Passport seria',
    'passport number' => 'Passport number',
    'pnfl' => 'PNFL',
    'birthdate' => 'Birthdate',
    'region' => "Region",
    'district' => 'District',
    'degree' => 'Education',
    'end_year_education' => 'Graduated year',
    'diplom_serial' => 'Diplom series',
    'diplom_number' => 'Diplom number',
    'ielts' => 'IELTS',
    'filial_id' => 'Filial',
    'department_id' => 'Faculty',
    'education_type_id' => 'Education type',
    'education_language_id' => 'Education language',
    'exam_day_place' => 'Exam day and place',
    'exam_time' => 'Exam time',
    'type end_year_education' => 'Type Graduated year, E: «YYYY»',
    'type diplom_serial' => 'Type Diplom series',
    'type diplom_number' => 'Type Diplom number',
    'type ielts' => "If you have IELTS, CEFR, National certificate, Olympiad diploma (.png, .jpg, .pdf), send it (if not, it is not mandatory)",
    'select' => 'Select...',
    'type fio' => "Type Surname Name Patronymic",
    'type passport seria' => 'Type Passport seria, E: «AA»',
    'type passport number' => 'Type  Passport number, E: «0000000»',
    'type pnfl' => 'Type PNFL, E: «00000000000000»',
    'type birthdate' => 'Type Birthdate, E: «dd.mm.YYYY»',
    'next step' => 'Next step',
    'previous step' => 'Previous step',
    'private information is incomplete' => 'Private information is incomplete',
    'educational information is incomplete' => 'Educational information is incomplete',
    'confirm oferta requirements' => 'Do you agree to the terms of the offer?',
    'yes' => 'Yes',
    'no' => 'No',
    'please read offer' => 'Please, read offer terms!',
    'to got next step accept offer' => 'To go to next step please accept offer terms.',
    'I am graduated of school this year' => 'I graduated this year',
    'general secondary education' => 'General secondary education',
    'medium special' => 'Medium special',
    'please upload file' => 'Please, upload a file!',
    'available file extensions' => "Available file extensions: png, jpg, pdf",
    'successfully uploaded' => 'Successfully uploaded!',
    'retry again' => 'Reupload again, please!',
    'download' => '⬇ Download',
    'please select exam day and place' => 'Please, select Exam day and Place',
    'please select region' => 'Please, select region',
    'your datas have been successfully saved' => 'Your datas have been successfully saved, We will wait you on the exam date!',
    'change phone' => '📱 Changing phone number',
    'personal informations' => 'Personal informations',
    'educational informations' => 'Educational informations',
    'exam informations' => 'Exam informations',
];
