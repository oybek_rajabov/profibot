<?php

namespace App\Hendlers;

use App\Models\BotUpdate;
use App\Models\Chat;
use App\Models\Department;
use App\Models\District;
use App\Models\EducationLanguage;
use App\Models\EducationType;
use App\Models\ExamDay;
use App\Models\ExamDayItem;
use App\Models\ExamPlace;
use App\Models\Filial;
use App\Models\Region;
use App\Models\User;
use App\Models\UserRegistration;
use App\Services\TelegramService;
use DefStudio\Telegraph\Facades\Telegraph;
use DefStudio\Telegraph\Keyboard\Button;
use DefStudio\Telegraph\Keyboard\Keyboard;
use DefStudio\Telegraph\Keyboard\ReplyButton;
use DefStudio\Telegraph\Keyboard\ReplyKeyboard;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class CustomWebhookHandler extends \DefStudio\Telegraph\Handlers\WebhookHandler
{
    protected function handleChatMessage(\Illuminate\Support\Stringable $text): void
    {
        app()->setLocale(BotUpdate::$user->lang);
        ///////////////////////////////////////////////STEP 1////////////////////////////////////
        if($text == __('bot.change phone')) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_1]);
            $this->requestContact();
            exit();
        }
        if (BotUpdate::$user->telegram_step == UserRegistration::STEP_1) {
            $phone = $this->message?->contact()?->phoneNumber() ?? $text;
            $phone = str_replace(['+', ' ', '-'], '', $phone);
            if (!TelegramService::getInstance()->validateNumber($phone)) {
                $this->chat->message('❌ ' . __('bot.wrong format'))->send();
            } else {
                if ($user = User::where('phone', $phone)->first()) {
                    $verification_code = generateCode();
                    sendSmsGetWay($phone, $verification_code);
                    BotUpdate::$user->update([
                        'phone' => $phone,
                        'telegram_step' => UserRegistration::STEP_2,
                        'verification_code' => $verification_code
                    ]);
                        $this->chat->message(__('bot.send verification code'))
                            ->replyKeyboard(ReplyKeyboard::make()
                                ->row([
                                    ReplyButton::make(__('bot.change phone'))
                                ])->oneTime()->resize()
                            )->send();
                        exit();
                }
                $verification_code = generateCode();
                sendSmsGetWay($phone, $verification_code);
                BotUpdate::$user->update([
                    'phone' => $phone,
                    'telegram_step' => UserRegistration::STEP_2,
                    'verification_code' => $verification_code
                ]);
                $this->chat->message(__('bot.send verification code'))->send();
                exit();
            }

        }
        //////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////STEP 2/////////////////////////////////////////////////
        $this->runStep(BotUpdate::$user->telegram_step);
    }

    public function start()
    {
        TelegramService::getInstance()->createOrUpdateChat($this->chat->chat_id);
        app()->setLocale('uz');
        $this->requestLanguage();

    }
    public function setLang()
    {
        app()->setLocale($this->data->get('lang'));
        BotUpdate::$user->update(['lang' => $this->data->get('lang')]);
        if (BotUpdate::$user->telegram_step == UserRegistration::STEP_1 || is_null(BotUpdate::$user->phone) || is_null(BotUpdate::$user->telegram_step)) {
            if (is_null(BotUpdate::$user->telegram_step))
                BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_1]);
            $this->requestContact();
            exit();
        }
        if(BotUpdate::$user->telegram_step == UserRegistration::STEP_2){
            $this->chat->message(__('bot.send verification code'))
                ->replyKeyboard(ReplyKeyboard::make()
                    ->row([
                        ReplyButton::make(__('bot.change phone'))
                    ])->oneTime()->resize()
                )->send();
             exit();
        }else{
            $this->chat->message(localLangSticker() . ' ' . app()->getLocale())->send();
            switch (TelegramService::getInstance()->checkWhichStep()){
                case 1:{$this->privateInformationsRequests();}; break;
                case 2:{$this->educationalInformationsRequests();}; break;
                case 3:{$this->examInformationRequests();}; break;
                case 4:{$this->profileInformations();}; break;
            }
        }

//        $this->runStep(BotUpdate::$user->telegram_step);
    }

    public function requestLanguage()
    {                                                
        $this->chat->message('👋 Tilni tanlang')
            ->keyboard(Keyboard::make()
                ->row([
                    Button::make(localLangSticker('uz') . "O'zbek")->action('setLang')->param('lang', 'uz'),
                    Button::make(localLangSticker('ru') . "Русский")->action('setLang')->param('lang', 'ru'),
                    Button::make(localLangSticker('en') . "English")->action('setLang')->param('lang', 'en'),
                ]))->send();
    }

    public function requestContact()
    {
        app()->setLocale(BotUpdate::$user->lang);
        $this->chat->message('📱 ' . __('bot.send your phone in format'))
            ->replyKeyboard(ReplyKeyboard::make()
                ->row([
                    ReplyButton::make("📱 " . __('bot.send phone'))->requestContact(),
                    ReplyButton::make(__('bot.change phone'))
                ])->oneTime()->resize()
            )->send();
    }

    public function privateInformationsRequests()
    {
        $this->checkAuth();
        app()->setLocale(BotUpdate::$user->lang);
        $this->chat->html("<b>" . __('bot.fio') . ":</b>   " . (is_null(BotUpdate::$user->fio) ? ' ❓' : BotUpdate::$user->fio . '  ✅') . "\n" .
            "<b>" . __('bot.passport seria') . ":</b>   " . (is_null(BotUpdate::$user->series) ? ' ❓' : BotUpdate::$user->series . '  ✅') . "\n" .
            "<b>" . __('bot.passport number') . ":</b>   " . (is_null(BotUpdate::$user->number) ? ' ❓' : BotUpdate::$user->number . '  ✅') . "\n" .
            "<b>" . __('bot.pnfl') . ":</b>   " . (is_null(BotUpdate::$user->jshshir) ? ' ❓' : BotUpdate::$user->jshshir . '  ✅') . "\n" .
            "<b>" . __('bot.birthdate') . ":</b>   " . (is_null(BotUpdate::$user->birthdate) ? ' ❓' : formatDate(BotUpdate::$user->birthdate) . '  ✅') . "\n" .
            "<b>" . __('bot.region') . ":</b>   " . (is_null(BotUpdate::$user->region_id) ? ' ❓' : BotUpdate::$user->region->name . '  ✅') . "\n" .
            "<b>" . __('bot.district') . ":</b>   " . (is_null(BotUpdate::$user->district_id) ? ' ❓' : BotUpdate::$user->district->name . '  ✅'))
            ->keyboard(Keyboard::make()
                ->row([
                    Button::make(__('bot.fio'))->action('privateInformations')->param('step', UserRegistration::STEP_3),
                ])
                ->row([
                    Button::make(__('bot.passport seria'))->action('privateInformations')->param('step', UserRegistration::STEP_4),
                    Button::make(__('bot.passport number'))->action('privateInformations')->param('step', UserRegistration::STEP_5),
                ])
                ->row([
                    Button::make(__('bot.pnfl'))->action('privateInformations')->param('step', UserRegistration::STEP_6),
                    Button::make(__('bot.birthdate'))->action('privateInformations')->param('step', UserRegistration::STEP_7),
                ])
                ->row([
                    Button::make(__('bot.region'))->action('privateInformations')->param('step', UserRegistration::STEP_8),
                    Button::make(__('bot.district'))->action('privateInformations')->param('step', UserRegistration::STEP_9),
                ])
                ->row([
                    Button::make(localLangSticker() . ' ' . __('bot.change language'))->action('privateInformations')->param('step', 50),
                    Button::make(__('bot.next step') . '➡')->action('privateInformations')->param('step', UserRegistration::STEP_10),
                ])

            )->send();
    }

    public function privateInformations()
    {
        $this->checkAuth();
        app()->setLocale(BotUpdate::$user->lang);
        if ($this->data->get('step') == UserRegistration::STEP_3) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_3]);
            $this->chat->message('👀 ' . __('bot.type fio'))->send();
        }
        if ($this->data->get('step') == UserRegistration::STEP_4) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_4]);
            $this->chat->message('👀 ' . __('bot.type passport seria'))->send();
        }
        if ($this->data->get('step') == UserRegistration::STEP_5) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_5]);
            $this->chat->message('👀 ' . __('bot.type passport number'))->send();
        }
        if ($this->data->get('step') == UserRegistration::STEP_6) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_6]);
            $this->chat->message('👀 ' . __('bot.type pnfl'))->send();
        }
        if ($this->data->get('step') == UserRegistration::STEP_7) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_7]);
            $this->chat->message('👀 ' . __('bot.type birthdate'))->send();
        }
        if ($this->data->get('step') == UserRegistration::STEP_8) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_8]);
            $regions = Region::all();
            $buttons = [];
            foreach ($regions as $region) {
                array_push($buttons, Button::make($region->name ?? '__')->action('selectRegion')->param('id', $region->id));
            }
            $this->chat->message('👀 ' . __('bot.select'))
                ->keyboard(Keyboard::make()
                    ->buttons($buttons)
                )->send();
        }
        if ($this->data->get('step') == UserRegistration::STEP_9) {
            if($region_id = BotUpdate::$user->region_id){
                $districts = District::query()->where('regions', $region_id)->get();
                $buttons = [];
                BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_8,'district_id' => null]);
                foreach ($districts as $district) {
                    array_push($buttons, Button::make($district->name)->action('selectDistrict')->param('id', $district->id));
                }
                $this->chat->message(__('bot.select'))
                    ->keyboard(Keyboard::make()
                        ->buttons($buttons)
                    )->send();
                exit();
            }
            $this->chat->message(__('bot.please select region'))->send();
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_8]);
        }
        if ($this->data->get('step') == UserRegistration::STEP_10) {
            if (TelegramService::getInstance()->checkFirstStep()) {
                BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_10, 'step' => UserRegistration::SITE_STEP_2]);
                $this->deleteMessages(10);
                $this->chat->message(__('bot.please read offer'))
                    ->document(env('SITE_URL') . '/storage/telegraph/oferta_' . app()->getLocale() . '.pdf')
                    ->send();
                $this->confirmOfertaRequest();
            } else {
                $this->chat->message(__('bot.private information is incomplete'))->send();
                BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
                $this->deleteMessages(10);
                $this->privateInformationsRequests();
                exit();
            }
        }
        if ($this->data->get('step') == 50) {
            $this->requestLanguage();
        }
    }

    public function educationalInformationsRequests()
    {
        $this->checkAuth();
        app()->setLocale(BotUpdate::$user->lang);
        $file_link = TelegramService::getInstance()->getFileLink();
        $link = '<a href="' . $file_link . '">' . __('bot.download') . '</a>';
        $this->chat->html("<b>" . __('bot.degree') . ":</b>   " . (is_null(BotUpdate::$user->degree) ? ' ❓' : TelegramService::getInstance()->getDegree(BotUpdate::$user->degree) . '  ✅') . "\n" .
            "<b>" . __('bot.end_year_education') . ":</b>   " . (is_null(BotUpdate::$user->end_year_education) ? ' ❓' : BotUpdate::$user->end_year_education . '  ✅') . "\n" .
            "<b>" . __('bot.diplom_serial') . ":</b>   " . (is_null(BotUpdate::$user->diplom_serial) ? ' ❓' : BotUpdate::$user->diplom_serial . '  ✅') . "\n" .
            "<b>" . __('bot.diplom_number') . ":</b>   " . (is_null(BotUpdate::$user->diplom_number) ? ' ❓' : BotUpdate::$user->diplom_number . '  ✅') . "\n" .
            "<b>" . __('bot.ielts') . ":</b>   " . (is_null(BotUpdate::$user->ielts) ? ' ' : $link . '  ✅'))->send();
        $this->chat->message(__('bot.select'))
            ->keyboard(Keyboard::make()
                ->row([
                    Button::make(__('bot.degree'))->action('educationalInformations')->param('step', UserRegistration::STEP_11),
                    Button::make(__('bot.end_year_education'))->action('educationalInformations')->param('step', UserRegistration::STEP_12),
                ])
                ->row([
                    Button::make(__('bot.diplom_serial'))->action('educationalInformations')->param('step', UserRegistration::STEP_13),
                    Button::make(__('bot.diplom_number'))->action('educationalInformations')->param('step', UserRegistration::STEP_14),
                ])
                ->row([
                    Button::make(__('bot.ielts'))->action('educationalInformations')->param('step', UserRegistration::STEP_15),
                ])
                ->row([
                    Button::make('⬅' . __('bot.previous step'))->action('educationalInformations')->param('step', UserRegistration::STEP_3),
                    Button::make(__('bot.next step') . '➡')->action('educationalInformations')->param('step', UserRegistration::STEP_16),
                ])
            )->send();
    }

    public function educationalInformations()
    {
        $this->checkAuth();
        app()->setLocale(BotUpdate::$user->lang);
        if ($this->data->get('step') == UserRegistration::STEP_11) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_11]);
            $this->chat->message('👀 ' . __('bot.select'))
                ->keyboard(Keyboard::make()
                    ->buttons([
                        Button::make(__('bot.I am graduated of school this year'))->action('selectDegree')->param('value', 3),
                        Button::make(__('bot.general secondary education'))->action('selectDegree')->param('value', 1),
                        Button::make(__('bot.medium special'))->action('selectDegree')->param('value', 2),
                    ])
                )->send();
        }
        if ($this->data->get('step') == UserRegistration::STEP_12) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_12]);
            $this->chat->message('👀 ' . __('bot.type end_year_education'))->send();
        }
        if ($this->data->get('step') == UserRegistration::STEP_13) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_13]);
            $this->chat->message('👀 ' . __('bot.type diplom_serial'))->send();
        }
        if ($this->data->get('step') == UserRegistration::STEP_14) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_14]);
            $this->chat->message('👀 ' . __('bot.type diplom_number'))->send();
        }
        if ($this->data->get('step') == UserRegistration::STEP_15) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_15]);
            $this->chat->message('👀 ' . __('bot.type ielts'))->send();
        }

        if ($this->data->get('step') == UserRegistration::STEP_16) {
            if (TelegramService::getInstance()->checkSecondtStep()) {
                BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100, 'step' => UserRegistration::SITE_STEP_3]);
                $this->examInformationRequests();
                exit();
            } else {
                $this->chat->message(__('bot.educational information is incomplete'))->send();
                BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
                $this->educationalInformationsRequests();
                exit();
            }
        }

        if ($this->data->get('step') == UserRegistration::STEP_3) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
            $this->privateInformationsRequests();
            exit();
        }
    }

    public function examInformationRequests()
    {
        $this->checkAuth();
        app()->setLocale(BotUpdate::$user->lang);
        $this->chat->html("<b>" . __('bot.filial_id') . ":</b>   " . (is_null(BotUpdate::$user->filial_id) ? ' ❓' : BotUpdate::$user?->filial?->name . '  ✅') . "\n" .
            "<b>" . __('bot.department_id') . ":</b>   " . (is_null(BotUpdate::$user->department_id) ? ' ❓' : BotUpdate::$user?->department?->name . '  ✅') . "\n" .
            "<b>" . __('bot.education_type_id') . ":</b>   " . (is_null(BotUpdate::$user->education_type_id) ? ' ❓' : BotUpdate::$user?->educationType?->name . '  ✅') . "\n" .
            "<b>" . __('bot.education_language_id') . ":</b>   " . (is_null(BotUpdate::$user->education_language_id) ? ' ❓' : BotUpdate::$user?->educationLanguage?->name . '  ✅') . "\n" .
            "<b>" . __('bot.exam_day_place') . ":</b>   " . (is_null(BotUpdate::$user?->examDayItem?->examDay) ? ' ❓' : formatDate(BotUpdate::$user?->examDayItem?->examDay->date) . ' (' . BotUpdate::$user?->examDayItem?->examDay->title . ')' . '  ✅') . "\n" .
            "<b>" . __('bot.exam_time') . ":</b>   " . (is_null(BotUpdate::$user?->examDayItem?->examPlace) ? ' ❓' : BotUpdate::$user?->examDayItem?->examPlace->name . ' (' . formatDateTime(BotUpdate::$user?->examDayItem?->exam_time, 'H:i') . ')' . '  ✅'))->send();
        $this->chat->message(__('bot.select'))
            ->keyboard(Keyboard::make()
                ->row([
                    Button::make(__('bot.filial_id'))->action('examInformations')->param('step', UserRegistration::STEP_16),
                    Button::make(__('bot.department_id'))->action('examInformations')->param('step', UserRegistration::STEP_17),
                ])
                ->row([
                    Button::make(__('bot.education_type_id'))->action('examInformations')->param('step', UserRegistration::STEP_18),
                    Button::make(__('bot.education_language_id'))->action('examInformations')->param('step', UserRegistration::STEP_19),
                ])
                ->row([
                    Button::make(__('bot.exam_day_place'))->action('examInformations')->param('step', UserRegistration::STEP_20),
                    Button::make(__('bot.exam_time'))->action('examInformations')->param('step', UserRegistration::STEP_21),
                ])
                ->row([
                    Button::make('⬅' . __('bot.previous step'))->action('examInformations')->param('step', UserRegistration::STEP_11),
                    Button::make(__('bot.next step') . '➡')->action('examInformations')->param('step', UserRegistration::STEP_22),
                ])
            )->send();
    }

    public function examInformations()
    {
        $this->checkAuth();
        app()->setLocale(BotUpdate::$user->lang);
        if ($this->data->get('step') == UserRegistration::STEP_16) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_16]);
            $filials = Filial::query()->orderBy('id')->get();
            $buttons = [];
            foreach ($filials as $filial) {
                array_push($buttons, Button::make($filial->name ?? '__')->action('selectFilial')->param('id', $filial->id));
            }
            $this->chat->message('👀 ' . __('bot.select'))
                ->keyboard(Keyboard::make()
                    ->buttons($buttons)
                )->send();
        }

        if ($this->data->get('step') == UserRegistration::STEP_17) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_17]);
            $faculties = Department::query()->orderBy('id')->get();
            $buttons = [];
            foreach ($faculties as $faculty) {
                array_push($buttons, Button::make($faculty->name ?? '__')->action('selectFaculty')->param('id', $faculty->id));
            }
            $this->chat->message('👀 ' . __('bot.select'))
                ->keyboard(Keyboard::make()
                    ->buttons($buttons)
                )->send();
        }

        if ($this->data->get('step') == UserRegistration::STEP_18) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_18]);
            $education_types = EducationType::query()->orderBy('id')->get();
            $buttons = [];
            foreach ($education_types as $education_type) {
                array_push($buttons, Button::make($education_type->name ?? '__')->action('selectEducationType')->param('id', $education_type->id));
            }
            $this->chat->message('👀 ' . __('bot.select'))
                ->keyboard(Keyboard::make()
                    ->buttons($buttons)
                )->send();
        }

        if ($this->data->get('step') == UserRegistration::STEP_19) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_19]);
            $education_languages = EducationLanguage::query()->orderBy('id')->get();
            $buttons = [];
            foreach ($education_languages as $education_language) {
                array_push($buttons, Button::make($education_language->name ?? '__')->action('selectEducationLanguage')->param('id', $education_language->id));
            }
            $this->chat->message('👀 ' . __('bot.select'))
                ->keyboard(Keyboard::make()
                    ->buttons($buttons)
                )->send();
        }

        if ($this->data->get('step') == UserRegistration::STEP_20) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_20]);
            $exam_day_items = ExamDayItem::query()->select(['exam_id'])
                ->withWhereHas('examDay', function ($query) {
                    $query->where('date','>=',date('Y-m-d'))
                    ->orderBy('date');
                })->groupBy('exam_id')->get();
            $buttons = [];
            foreach ($exam_day_items as $exam_day_item) {
                array_push($buttons, Button::make(formatDate($exam_day_item?->examDay?->date) . ' (' . $exam_day_item?->examDay?->title . ')' ?? '__')->action('selectExamDay')->param('exam_id', $exam_day_item->exam_id));
            }
            $this->chat->message('👀 ' . __('bot.select'))
                ->keyboard(Keyboard::make()
                    ->buttons($buttons)
                )->send();
        }

        if ($this->data->get('step') == UserRegistration::STEP_21) {
            if (BotUpdate::$user->exam_item_id) {
                BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_21]);
                $exam_day_items = ExamDayItem::query()
                    ->where('exam_id', BotUpdate::$user->examDayItem->exam_id)
                    ->withWhereHas('examDay', function ($query) {
                        $query->where('date','>=',date('Y-m-d'))
                            ->orderBy('date');
                    })
                    ->with('examPlace')
                    ->orderBy('exam_time')->get();
                $buttons = [];
                foreach ($exam_day_items as $exam_day_item) {
                    array_push($buttons, Button::make($exam_day_item?->examPlace->name . '' . '(' . formatDateTime($exam_day_item->exam_time, 'H:i') . ')' ?? '__')->action('selectExamPlace')->param('id', $exam_day_item->id));
                }
                $this->chat->message('👀 ' . __('bot.select'))
                    ->keyboard(Keyboard::make()
                        ->buttons($buttons)
                    )->send();
                exit();
            } else {
                BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_20]);
                $this->chat->message(__('bot.please select exam day and place'))->send();
                exit();
            }

        }

        if ($this->data->get('step') == UserRegistration::STEP_22) {
            if (TelegramService::getInstance()->checkThirdStep()) {
                BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_22, 'registration_date' => date('Y-m-d'), 'step' => UserRegistration::SITE_STEP_4]);
                $this->chat->message(__('bot.your datas have been successfully saved'))->send();
                $this->profileInformations();
                BotUpdate::$user->update(['step' => UserRegistration::SITE_STEP_5]);
                exit();
            } else {
                $this->chat->message(__('bot.educational information is incomplete'))->send();
                BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
                $this->examInformationRequests();
                exit();
            }
        }

        if ($this->data->get('step') == UserRegistration::STEP_11) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
            $this->educationalInformationsRequests();
        }
    }

    public function runStep($step)
    {
        $this->checkAuth();
        if ($step == UserRegistration::STEP_2) {
            if ($this?->message?->text() == BotUpdate::$user->verification_code) {
                if($user = User::where('phone',BotUpdate::$user->phone)->whereNot('id',BotUpdate::$user->id)->first()){
                    $user->update(['telegram_id' => BotUpdate::$chat_id,'lang' => BotUpdate::$user->lang]);
                    BotUpdate::$user->delete();
                    BotUpdate::changeUser($user);
                    if(BotUpdate::$user->step < 0)
                        BotUpdate::$user->update(['step' => 2]);
                    switch (TelegramService::getInstance()->checkWhichStep()){
                        case 4 : $this->profileInformations(); break;
                        case 3 : $this->examInformationRequests(); break;
                        case 2 : $this->educationalInformationsRequests(); break;
                        case 5 : $this->privateInformationsRequests(); break;
                        default: $this->privateInformationsRequests();
                    }
                    exit();
                }
                BotUpdate::$user->update([
                    'telegram_step' => UserRegistration::STEP_3, 'step' => 2
                ]);
                $this->chat->message(__('bot.send your private informations'))
                    ->replyKeyboard(ReplyKeyboard::make()
                        ->row([
                            ReplyButton::make(__('bot.change phone'))
                        ])->oneTime()->resize()
                    )->send();
                $this->privateInformationsRequests();
                BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
            } else {
                $this->chat->message(__('bot.code is not correct'))->send();
                exit();
            }
        }

        if ($step == UserRegistration::STEP_3) {
            BotUpdate::$user->update([
                'fio' => $this?->message?->text()
            ]);
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
            $this->privateInformationsRequests();
            exit();
        }

        if ($step == UserRegistration::STEP_4) {
            if (!TelegramService::getInstance()->validatePassportSeria($this?->message?->text())) {
                $this->chat->message('❌ ' . __('bot.wrong format'))->send();
            } else {
                BotUpdate::$user->update([
                    'series' => $this?->message?->text()
                ]);
                BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
                $this->privateInformationsRequests();
                exit();
            }
        }

        if ($step == UserRegistration::STEP_5) {
            if (!TelegramService::getInstance()->validatePassportNumber($this?->message?->text())) {
                $this->chat->message('❌ ' . __('bot.wrong format'))->send();
            } else {
                BotUpdate::$user->update([
                    'number' => $this?->message?->text(),
                ]);
                BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
                $this->privateInformationsRequests();
                exit();
            }
        }

        if ($step == UserRegistration::STEP_6) {
            if (!TelegramService::getInstance()->validateJshshir($this?->message?->text())) {
                $this->chat->message('❌ ' . __('bot.wrong format'))->send();
            } else {
                BotUpdate::$user->update([
                    'jshshir' => $this?->message?->text(),
                    'password' => Hash::make($this?->message?->text())
                ]);
                BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
                $this->privateInformationsRequests();
                exit();
            }
        }

        if ($step == UserRegistration::STEP_7) {
            if (TelegramService::getInstance()->validateBirthdate($this->message?->text())) {
                $this->chat->message('❌ ' . __('bot.wrong format'))->send();
                exit();
            } else {
                BotUpdate::$user->update([
                    'birthdate' => formatDate($this?->message?->text(), 'Y-m-d'),
                ]);
                BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
                $this->privateInformationsRequests();
                exit();
            }
        }
        if ($step == UserRegistration::STEP_7 || $step == UserRegistration::STEP_8) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
            $this->privateInformationsRequests();
            exit();
        }

        if ($step == UserRegistration::STEP_11) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
            $this->educationalInformationsRequests();
            exit();
        }

        if ($step == UserRegistration::STEP_12) {
            if (TelegramService::getInstance()->validationYear($this->message?->text())) {
                BotUpdate::$user->update([
                    'end_year_education' => $this?->message?->text()
                ]);
            } else {
                $this->chat->message('❌ ' . __('bot.wrong format'))->send();
                exit();
            }
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
            $this->educationalInformationsRequests();
            exit();
        }

        if ($step == UserRegistration::STEP_13) {
            BotUpdate::$user->update([
                'diplom_serial' => $this?->message?->text()
            ]);
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
            $this->educationalInformationsRequests();
            exit();
        }
        if ($step == UserRegistration::STEP_14) {
            BotUpdate::$user->update([
                'diplom_number' => $this?->message?->text()
            ]);
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
            $this->educationalInformationsRequests();
            exit();
        }
        if ($step == UserRegistration::STEP_15) {
            $message = $this->request->message;
            if (isset($message['document']['file_id'])) {
                $result = TelegramService::getInstance()->uploadFile($message['document']['file_id']);
                $this->chat->message($result)->send();
                BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
                $this->educationalInformationsRequests();
                exit();
            }
            if (isset($message['photo'][2]['file_id']) || isset($message['photo'][3]['file_id'])) {
                $file_id = $message['photo'][2]['file_id'] ?? $message['photo'][3]['file_id'];
                $result = TelegramService::getInstance()->uploadFile($file_id);
                $this->chat->message($result)->send();
                BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
                $this->educationalInformationsRequests();
                exit();
            }
            $this->chat->message(__('bot.please upload file'))->send();
        }

        if ($step >= UserRegistration::STEP_16 && $step <= UserRegistration::STEP_21) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
            $this->examInformationRequests();
            exit();
        }

    }

    public function deleteMessages($count)
    {
        $this->checkAuth();
        $back = 1;
        for ($i = 1; $i <= $count; $i++) {
            $this->chat->deleteMessage($this->messageId - $back)->send();
            $back = $back + 1;
        }
    }

    public function selectRegion()
    {
        $this->checkAuth();
        app()->setLocale(BotUpdate::$user->lang);
        $region_id = $this->data->get('id');
        $districts = District::query()->where('regions', $region_id)->get();
        $buttons = [];
        BotUpdate::$user->update(['region_id' => $region_id, 'telegram_step' => UserRegistration::STEP_8,'district_id' => null]);
        foreach ($districts as $district) {
            array_push($buttons, Button::make($district->name)->action('selectDistrict')->param('id', $district->id));
        }
        $this->chat->message(__('bot.select'))
            ->keyboard(Keyboard::make()
                ->buttons($buttons)
            )->send();
    }

    public function selectDistrict()
    {
        $this->checkAuth();
        $district_id = $this->data->get('id');
        BotUpdate::$user->update(['district_id' => $district_id]);
        $this->privateInformationsRequests();
    }

    public function selectDegree()
    {
        $this->checkAuth();
        app()->setLocale(BotUpdate::$user->lang);
        BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_11, 'degree' => $this->data->get('value')]);
        $this->educationalInformationsRequests();
    }

    public function confirmOfertaRequest()
    {
        $this->checkAuth();
        $this->chat->message(__('bot.confirm oferta requirements'))
            ->keyboard(Keyboard::make()
                ->row([
                    Button::make(__('bot.yes'))->action('acceptOferta')->param('param', 1),
                    Button::make(__('bot.no'))->action('acceptOferta')->param('param', 0),
                ])
            )->send();
    }

    public function acceptOferta()
    {
        $this->checkAuth();
        app()->setLocale(BotUpdate::$user->lang);
        if ($this->data->get('param') == 1) {
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_11, 'step' => UserRegistration::SITE_STEP_2]);
            $this->runStep(UserRegistration::STEP_11);
        }
        if ($this->data->get('param') == 0) {
            $this->chat->message(__('bot.to got next step accept offer'))->send();
        }
    }

    public function selectFilial()
    {
        $this->checkAuth();
        app()->setLocale(BotUpdate::$user->lang);
        BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_17, 'filial_id' => $this->data->get('id')]);
        BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
        $this->examInformationRequests();
    }

    public function selectFaculty()
    {
        $this->checkAuth();
        app()->setLocale(BotUpdate::$user->lang);
        BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_18, 'department_id' => $this->data->get('id')]);
        BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
        $this->examInformationRequests();
    }

    public function selectEducationType()
    {
        $this->checkAuth();
        app()->setLocale(BotUpdate::$user->lang);
        BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_19, 'education_type_id' => $this->data->get('id')]);
        BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
        $this->examInformationRequests();
    }

    public function selectEducationLanguage()
    {
        $this->checkAuth();
        app()->setLocale(BotUpdate::$user->lang);
        BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_20, 'education_language_id' => $this->data->get('id')]);
        BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_100]);
        $this->examInformationRequests();
    }

    public function selectExamDay()
    {
        $this->checkAuth();
        app()->setLocale(BotUpdate::$user->lang);
        $examDayItem = ExamDayItem::where('exam_id', $this->data->get('exam_id'))->first();
        $exam_day_items = ExamDayItem::query()
            ->where('exam_id', $examDayItem->exam_id)
            ->withWhereHas('examDay', function ($query) {
                $query->where('date','>=',date('Y-m-d'))
                    ->orderBy('date');
            })
            ->with('examPlace')
            ->orderBy('exam_time')->get();
        $buttons = [];
        foreach ($exam_day_items as $exam_day_item) {
            array_push($buttons, Button::make($exam_day_item?->examPlace?->name . '' . ' (' . formatDateTime($exam_day_item->exam_time, 'H:i') . ')' ?? '__')->action('selectExamPlace')->param('id', $exam_day_item->id));
        }
        $this->chat->message('👀 ' . __('bot.select'))
            ->keyboard(Keyboard::make()
                ->buttons($buttons)
            )->send();
    }

    public function selectExamPlace()
    {
        $this->checkAuth();
        app()->setLocale(BotUpdate::$user->lang);
        BotUpdate::$user->update([
            'exam_item_id' => $this->data->get('id'),
            'telegram_step' => UserRegistration::STEP_100
        ]);
        $this->examInformationRequests();
    }

    public function profileInformations()
    {
        $this->checkAuth();
        app()->setLocale(BotUpdate::$user->lang);
        $file_link = TelegramService::getInstance()->getFileLink();
        $link = '<a href="' . $file_link . '">' . __('bot.download') . '</a>';
        $this->chat->html(
            "----<b>👤" . __('bot.personal informations') . ":</b>----" . "\n" .
            "<b>" . __('bot.fio') . ":</b>   " . (is_null(BotUpdate::$user->fio) ? ' ❓' : BotUpdate::$user->fio) . "\n" .
            "<b>" . __('bot.passport seria') . ":</b>   " . (is_null(BotUpdate::$user->series) ? ' ❓' : BotUpdate::$user->series) . "\n" .
            "<b>" . __('bot.passport number') . ":</b>   " . (is_null(BotUpdate::$user->number) ? ' ❓' : BotUpdate::$user->number) . "\n" .
            "<b>" . __('bot.pnfl') . ":</b>   " . (is_null(BotUpdate::$user->jshshir) ? ' ❓' : BotUpdate::$user->jshshir) . "\n" .
            "<b>" . __('bot.birthdate') . ":</b>   " . (is_null(BotUpdate::$user->birthdate) ? ' ❓' : formatDate(BotUpdate::$user->birthdate)) . "\n" .
            "<b>" . __('bot.region') . ":</b>   " . (is_null(BotUpdate::$user->region_id) ? ' ❓' : BotUpdate::$user->region->name) . "\n" .
            "<b>" . __('bot.district') . ":</b>   " . (is_null(BotUpdate::$user->district_id) ? ' ❓' : BotUpdate::$user->district->name) . "\n" .
            "\n\n----<b>📚" . __('bot.educational informations') . ":</b>----" . "\n" .
            "<b>" . __('bot.degree') . ":</b>   " . (is_null(BotUpdate::$user->degree) ? ' ❓' : TelegramService::getInstance()->getDegree(BotUpdate::$user->degree)) . "\n" .
            "<b>" . __('bot.end_year_education') . ":</b>   " . (is_null(BotUpdate::$user->end_year_education) ? ' ❓' : BotUpdate::$user->end_year_education) . "\n" .
            "<b>" . __('bot.diplom_serial') . ":</b>   " . (is_null(BotUpdate::$user->diplom_serial) ? ' ❓' : BotUpdate::$user->diplom_serial) . "\n" .
            "<b>" . __('bot.diplom_number') . ":</b>   " . (is_null(BotUpdate::$user->diplom_number) ? ' ❓' : BotUpdate::$user->diplom_number) . "\n" .
            "<b>" . __('bot.ielts') . ":</b>   " . (is_null(BotUpdate::$user->ielts) ? '' : $link) . "\n" .
            "\n\n----<b>🏢" . __('bot.exam informations') . ":</b>----" . "\n" .
            "<b>" . __('bot.filial_id') . ":</b>   " . (is_null(BotUpdate::$user->filial_id) ? ' ❓' : BotUpdate::$user?->filial?->name) . "\n" .
            "<b>" . __('bot.department_id') . ":</b>   " . (is_null(BotUpdate::$user->department_id) ? ' ❓' : BotUpdate::$user?->department?->name) . "\n" .
            "<b>" . __('bot.education_type_id') . ":</b>   " . (is_null(BotUpdate::$user->education_type_id) ? ' ❓' : BotUpdate::$user?->educationType?->name) . "\n" .
            "<b>" . __('bot.education_language_id') . ":</b>   " . (is_null(BotUpdate::$user->education_language_id) ? ' ❓' : BotUpdate::$user?->educationLanguage?->name) . "\n" .
            "<b>" . __('bot.exam_day_place') . ":</b>   " . (is_null(BotUpdate::$user?->examDayItem?->examDay) ? ' ❓' : formatDate(BotUpdate::$user?->examDayItem?->examDay->date) . ' (' . BotUpdate::$user?->examDayItem?->examDay->title . ')') . "\n" .
            "<b>" . __('bot.exam_time') . ":</b>   " . (is_null(BotUpdate::$user?->examDayItem?->examPlace) ? ' ❓' : BotUpdate::$user?->examDayItem?->examPlace->name . ' (' . formatDateTime(BotUpdate::$user?->examDayItem?->exam_time, 'H:i') . ')'))->send();
    }

    public function checkAuth()
    {
        if(BotUpdate::$user->phone == null || BotUpdate::$user->phone == ''){
            BotUpdate::$user->update(['telegram_step' => UserRegistration::STEP_1]);
            $this->requestContact();
            exit();
        }
    }
}
