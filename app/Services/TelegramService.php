<?php

namespace App\Services;

use App\Models\BotUpdate;
use App\Models\Chat;
use App\Models\MyBot;
use App\Repository\TelegramRepository;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;


class TelegramService
{
    public $repository;

    public function __construct(TelegramRepository $data)
    {
        $this->repository = $data;
    }

    public static function getInstance(): TelegramService
    {
        return new static(TelegramRepository::getInctance());
    }

    public function requestContact($chat)
    {
//        $chat->keyboard(Keyboard::make()->buttons([
//            Button::make('Delete')->action('delete')->param('id', '42'),
//        ]))->send();
//
//        $chat->message('Телефон рақамингизни +998 ** *** **** шаклида юборинг, ёки "📱 Рақам жўнатиш" тугмасини босинг:')
//            ->replyKeyboard(ReplyKeyboard::make()
//                ->button("📱 Raqam jo'natish")->requestContact()
//                ->button(localLangSticker().' '.__('bot.change language'))
//                ->deleteButton()
//                ->oneTime()
//            )->send();
    }

    public function createOrUpdateChat($chat_id)
    {
        if (!Chat::query()->where('chat_id', $chat_id)->exists())
            Chat::create([
                'chat_id' => $chat_id,
                'name' => "Chat #$chat_id",
            ]);
    }

    public function registerWebhook()
    {
        try {
            $MyBot = MyBot::query()->first();
            $MyBot->registerWebhook()->send();
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => $e->getMessage()]);
        }
        return true;
    }

    public function unRegisterWebhook()
    {
        try {
            $MyBot = MyBot::query()->first();
            $MyBot->unregisterWebhook()->send();
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => $e->getMessage()]);
        }
        return true;
    }

    public function getContact($Message)
    {
        return $Message?->message()?->contact()?->phoneNumber() ?? $Message->message()->text();
    }

    public function deleteLastMessage($chat, $Message)
    {
        \DefStudio\Telegraph\Facades\Telegraph::chat($chat)->deleteMessage($Message->message()->id());
    }

    public function validateNumber($phone)
    {
        $subject = $phone;
        $pattern_phone_number = '/^998[0-9]{9}$/';
        $pattern_a_z = '/[a-z]$/';
        preg_match($pattern_a_z, $subject, $matches_a_z, PREG_OFFSET_CAPTURE);
        if (!$matches_a_z) {
            preg_match($pattern_phone_number, $subject, $matches__phone_number, PREG_OFFSET_CAPTURE);
            if ($matches__phone_number) {
                return true;
            } else return false;
        }
        return false;
    }

    public function validatePassportSeria($text)
    {
        $seria = $text;
        $pattern = '/^[A-Z]{2}$/';
        preg_match($pattern, $seria, $result, PREG_OFFSET_CAPTURE);
        if ($result)
            return true;
        else return false;

    }

    public function validatePassportNumber($text)
    {
        $seria = $text;
        $pattern = '/^[0-9]{7}$/';
        preg_match($pattern, $seria, $result, PREG_OFFSET_CAPTURE);
        if ($result)
            return true;
        else return false;

    }

    public function validateJshshir($text)
    {
        $seria = $text;
        $pattern = '/^[0-9]{14}$/';
        preg_match($pattern, $seria, $result, PREG_OFFSET_CAPTURE);
        if ($result)
            return true;
        else return false;

    }

    public function validateBirthdate($text)
    {
        $validation = \Illuminate\Support\Facades\Validator::make(['birthdate' => $text], [
            'birthdate' => ['required', 'date', 'date_format:d.m.Y']
        ]);
        if ($validation->fails())
            return true;
        else return false;
    }

    public function validationYear($text)
    {
        if (is_numeric($text) && (intval($text) > 1900 && intval($text) <= intval(date('Y'))))
            return true;
        else return false;
    }

    public function checkFirstStep()
    {
        if (is_null(BotUpdate::$user->fio)) return false;
        if (is_null(BotUpdate::$user->series)) return false;
        if (is_null(BotUpdate::$user->number)) return false;
        if (is_null(BotUpdate::$user->jshshir)) return false;
        if (is_null(BotUpdate::$user->birthdate)) return false;
        if (is_null(BotUpdate::$user->region_id)) return false;
        if (is_null(BotUpdate::$user->district_id)) return false;
        return true;
    }

    public function checkSecondtStep()
    {
        if (is_null(BotUpdate::$user->degree)) return false;
        if (is_null(BotUpdate::$user->end_year_education)) return false;
        if (is_null(BotUpdate::$user->diplom_serial)) return false;
        if (is_null(BotUpdate::$user->diplom_number)) return false;
        return true;
    }
    public function checkThirdStep()
    {
        if (is_null(BotUpdate::$user->filial_id)) return false;
        if (is_null(BotUpdate::$user->department_id)) return false;
        if (is_null(BotUpdate::$user->education_type_id)) return false;
        if (is_null(BotUpdate::$user->education_language_id)) return false;
        if (is_null(BotUpdate::$user->exam_item_id)) return false;
        return true;
    }

    public function checkWhichStep(){
        if($this->checkFirstStep() && $this->checkSecondtStep() && $this->checkThirdStep())
            return 4;
        if($this->checkFirstStep() && $this->checkSecondtStep())
            return 3;
        if($this->checkFirstStep())
            return 2;
        else return 1;
    }


    public function getDegree($value)
    {
        app()->setLocale(BotUpdate::$user->lang);
        switch ($value) {
            case 1:
                return __('bot.general secondary education');
                break;
            case 2:
                return __('bot.medium special');
                break;
            case 3:
                return __('bot.I am graduated of school this year');
                break;
            default:
                return '';
        }
    }

    public function uploadFile($telegram_file_id)
    {
        $result = Http::get("https://api.telegram.org/bot".env('BOT_TOKEN')."/getFile?file_id={$telegram_file_id}")->object();
        if($result->ok){
            $extension = pathinfo($result->result->file_path, PATHINFO_EXTENSION);
           if($extension == 'png' || $extension == 'jpg' || $extension == 'pdf')
           {
               $file_name = 'certificate_'.strtotime(date('d.m.Y H:i:s')).BotUpdate::$user->id.'.'.$extension;
               $path = 'public/telegraph/certificates';
               /* make folder */
               File::isDirectory(storage_path($path)) or File::makeDirectory($path, 0777, true, true);
               if(Storage::put($path.'/'.$file_name,Http::get('https://api.telegram.org/file/bot'.env('BOT_TOKEN').'/'.$result->result->file_path))){
                     $file = $this->getFile('storage/telegraph/certificates/'.$file_name,'ielts_certificates');
                   $imagePath = uploadImage($file,'ielts_certificates');
                   BotUpdate::$user->update(['ielts' => $imagePath]);
                   unlink('storage/telegraph/certificates/'.$file_name);
                   return __('bot.successfully uploaded');
               }
           }else return __('bot.available file extensions');
        }else return __('bot.retry again');
    }

    public function getFileLink()
    {
       return BotUpdate::$user->ielts? config('params.get_file_server').BotUpdate::$user->ielts : '';
    }

    public function assignTelegramStep()
    {
        switch (BotUpdate::$user->step){
            case 3: BotUpdate::$user->update(['telegram_step' => 11]); break;
            case 4: BotUpdate::$user->update(['telegram_step' => 16]); break;
            case 5: BotUpdate::$user->update(['telegram_step' => 22]); break;
            case 2: BotUpdate::$user->update(['telegram_step' => 3]); break;
        }
    }


    public function getFile($path)
    {
        $name = File::name( $path );

        $extension = File::extension( $path );

        $originalName = $name . '.' . $extension;

        $mimeType = File::mimeType( $path );

        $size = File::size( $path );

        $error = false;

        $object = new UploadedFile($path,$originalName,$mimeType,$error);

        return $object;
    }

    public function checkAuth()
    {

    }
}
