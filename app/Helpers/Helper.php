<?php
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

function localLangSticker($lang = ''){
        if($lang)
        switch ($lang){
            case 'en': return "🏴󠁧󠁢󠁥󠁮󠁧󠁿󠁮󠁧󠁿"; break;
            case 'ru': return "🇷🇺"; break;
            case 'uz': return "🇺🇿"; break;
        }
        else  switch (app()->getLocale()){
            case 'en': return "🏴󠁧󠁢󠁥󠁮󠁧󠁿󠁮󠁧󠁿"; break;
            case 'ru': return "🇷🇺"; break;
            case 'uz': return "🇺🇿"; break;
        }
    }

function formatDate($date_string, $format = 'd.m.Y')
{
    if (empty($date_string)) {
        return '';
    }
    return date($format, strtotime($date_string));
}

function formatDateTime($date_string, $format = 'd.m.Y H:i')
{
    if (empty($date_string)) {
        return '';
    }
    return date($format, strtotime($date_string));
}

function fileSaveOriginal($file, $folder, $fileName, $oldFile = null): bool
{
    $path = storage_path() . '/app/public';
    $pathNmae = $path . '/' . $folder; // file path  , folder name
    $pathSave = 'public/' . $folder . '/'; // file path  , folder name

    /* old image deleted */
    if ($oldFile && file_exists($pathNmae . '/' . $oldFile)) {
        unlink($pathNmae . '/' . $oldFile);
    }

    /* make folder */
    File::isDirectory($pathNmae) or File::makeDirectory($pathNmae, 0777, true, true);
    try {
        /* file saved */
        $file->storeAs($pathSave, $fileName);
        return true;
    } catch (Exception $e) {
        return true;
    }
}

function sendSmsGetWay($phone, $code)
{
    $phone = str_replace(['+',' ','-'], '', $phone);
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, 'http://185.8.212.184/smsgateway/');
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($curl, CURLOPT_TIMEOUT, 5);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_POSTFIELDS, 'login=' . urlencode('Profiuni') . '&password=' . urlencode('01D16oAml608jE0vk58M') . '&data=' . urlencode('[ {"phone":"' . $phone . '", "text":"Profiuniversity.uz ' . $code . '"} ]'));
    $res = curl_exec($curl);
    $error = curl_error($curl);
    curl_close($curl);
    if($error){
        return false;
    } else {
        return true;
    }
}

function generateCode(){
        return mt_rand(1000,9999);
}

function randomUserId()
{
    $check = true;
    $random = [];
    while ($check) {
        $random = ['user_id' => rand(100000, 999999)];
        $check = Validator::make($random, [
            'user_id' => 'unique:users'
        ]);
        if ($check->fails()) {
            $check = true;
        } else {
            $check = false;
        }
    }
    return $random['user_id'];
}

function uploadImage($file, $path, $old = null, $sub_path = null)
{
    if ($old != null && file_exists(public_path() . '/' . $old)) {
        unlink(public_path() . '/' . $old);
    }
    $image = time() . '_file' . '.' . $file->getClientOriginalExtension();
    uploadFileServer($file, $image, $path, $old);
//        if (is_null($sub_path)) {
//            $file->storeAs("public/$path/", $image);
//            return "storage/$path/" . $image;
//        } else {
//            $file->storeAs("public/$path/$sub_path/", $image);
//            return "storage/$path/$sub_path/" . $image;
//        }

    return "storage/$path/" . $image;
}


 function uploadFileServer($file, $fileName, $path, $oldFileName = null)
{
    $file = new \CURLFile($file);
    $fields_string = [
        'file'          => $file,
        'fileName'      => $fileName,
        'folderName'    => $path,
        'oldFilename'   => $oldFileName,
    ];
    $url = config('params.upload_file_server');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: multipart/form-data']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}
