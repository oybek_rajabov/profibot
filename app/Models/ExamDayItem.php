<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class ExamDayItem extends  Model
{
    protected $table = 'exam_day_items';
    protected $guarded = [];

    public function examDay()
    {
        return $this->hasOne(ExamDay::class,'id','exam_id');
    }

    public function examPlace()
    {
        return $this->hasOne(ExamPlace::class,'id','exam_place_id');
    }
}
