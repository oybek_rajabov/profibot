<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends  Model
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $table = 'users';
    protected $guarded = [];

    public function district()
    {
        return $this->hasOne(District::class,'id','district_id');
    }

    public function region()
    {
        return $this->hasOne(Region::class,'id','region_id');
    }

    public function department()
    {
        return $this->hasOne(Department::class,'id','department_id');
    }

    public function educationType()
    {
        return $this->hasOne(EducationType::class,'id','education_type_id');
    }

    public function educationLanguage()
    {
        return $this->hasOne(EducationLanguage::class,'id','education_language_id');
    }

    public function filial()
    {
        return $this->hasOne(Filial::class,'id','filial_id');
    }

    public function examDayItem()
    {
        return $this->hasOne(ExamDayItem::class,'id','exam_item_id')
            ->with('examDay')
            ->with('examPlace');
    }

}
