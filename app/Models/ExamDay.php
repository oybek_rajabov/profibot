<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class ExamDay extends  Model
{
    protected $table = 'exam_days';
    protected $guarded = [];

    public function getTitleAttribute($value)
    {
        return $this->translateName->field_value ?? ($value ?? '');
    }

    public static function getLangKeyName()
    {
        return ['title'];
    }

    public function translateTitle()
    {
        return $this->hasOne(Translate::class,'field_id','id')
            ->where('table_name',$this->getTable())
            ->where('field_name','title')
            ->where('language_code',app()->getLocale());
    }

}
