<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class UserRegistration extends Model
{
    protected $table = 'users';
    protected $guarded = [];
    ///////first step/////////
    const STEP_1 = 1; // start
    const STEP_2 = 2;  // insert phone
    const STEP_3 = 3;  // fio
    const STEP_4 = 4;  // passport seria
    const STEP_5 = 5;  // passport number
    const STEP_6 = 6;  // JSHSHIR
    const STEP_7 = 7;  // birthdate
    const STEP_8 = 8;  // region
    const STEP_9 = 9;  // district
    const STEP_10 = 10;  // oferta
    /////////////second step//////////////
    const STEP_11 = 11; // degree(Education)
    const STEP_12 = 12; // end_year_education
    const STEP_13 = 13; // diplom_serial
    const STEP_14 = 14; // diplom_number
    const STEP_15 = 15; // IELTS
    /////////////third step////////////////
    const STEP_16 = 16; // check second step and select Filial
    const STEP_17 = 17; // select Faculty
    const STEP_18 = 18; // select Education type
    const STEP_19 = 19; // select Education language
    const STEP_20 = 20; // select exam day place
    const STEP_21 = 21; // select exam time
    /////////////////
    const STEP_22 = 22; // Conclusion
    const STEP_100 = 100; // pause

    const SITE_STEP_1 = 1; //[1,2]verification phone
    const SITE_STEP_2 = 2; //[3,10] private informations
    const SITE_STEP_3 = 3; //[11,15]educational informations
    const SITE_STEP_4 = 4; //[16,21]exam informations
    const SITE_STEP_5 = 5; //[22] end

    public static function getUser($chat_id)
    {
        $model = UserRegistration::query()->where('telegram_id',$chat_id)->whereNot('telegram_step','=',8)->first();
         if($model)
             return  $model;
         else{
             return UserRegistration::create([
                 'telegram_step' => UserRegistration::STEP_1,
                 'telegram_id' => $chat_id
             ]);
         }

    }

}
