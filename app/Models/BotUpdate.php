<?php

namespace App\Models;

use DefStudio\Telegraph\Models\TelegraphBot;
use Illuminate\Support\Facades\Log;

class BotUpdate
{
    public static $message_id;
    public static $chat_id;
    public static $userName;
    public static $user;

    public function __construct($request)
    {
        $requestArray = $request->all();
        if (array_key_exists('message', $requestArray)) {
            $requestArray = $request->all();
            self::$message_id = $requestArray['message']['message_id'];
            self::$chat_id = $requestArray['message']['chat']['id'];
            self::$userName = $requestArray['message']['chat']['username'] ?? '';
            self::setUser($requestArray['message']['chat']['id']);
        } else {
            $requestArray = $request->all();
            self::$message_id = $requestArray['callback_query']['message']['message_id'];
            self::$chat_id = $requestArray['callback_query']['message']['chat']['id'];
            self::$userName = $requestArray['callback_query']['message']['chat']['username'] ?? '';
            self::setUser($requestArray['callback_query']['message']['chat']['id']);
        }
    }

    public static function setUser($chat_id, $step = UserRegistration::STEP_1)
    {
        $user = User::query()->where('telegram_id', $chat_id)->first();
        if (!$user) {
            $studentRole = Role::query()->where('key','student')->first();
            $user = User::query()->forceCreate([
                'telegram_id' => $chat_id,
                'telegram_step' => $step,
                'lang' => 'uz',
                'role_id' => $studentRole->id,
                'user_id' => randomUserId()
            ]);
        }
        self::$user = $user;
    }

    public static function changeUser(User $user)
    {
        self::$user = $user;
    }


}
