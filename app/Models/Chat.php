<?php

namespace App\Models;

use DefStudio\Telegraph\Models\TelegraphBot;
use DefStudio\Telegraph\Models\TelegraphChat;

class Chat extends TelegraphChat
{
    protected $table = 'telegraph_chats';
}
