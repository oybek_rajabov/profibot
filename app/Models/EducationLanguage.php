<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class EducationLanguage extends  Model
{
    protected $table = 'education_languages';
    protected $guarded = [];

    public function getNameAttribute($value)
    {
        return $this->translateName->field_value ?? ($value ?? '');
    }

    public static function getLangKeyName()
    {
        return ['name'];
    }

    public function translateName()
    {
        return $this->hasOne(Translate::class,'field_id','id')
            ->where('table_name',$this->getTable())
            ->where('field_name','name')
            ->where('language_code',app()->getLocale());
    }

}
