<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array<int, string>
     */
    protected $except = [
        '/5839880048:AAFb5BCH_RRxsf1qrJww7BW3DOKOnKnKqgU/https://testbot.rasmlar.net/api/bot',
    ];
}
