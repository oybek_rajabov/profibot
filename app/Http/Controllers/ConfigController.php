<?php

namespace App\Http\Controllers;

use App\Http\Requests\Config\CreateBotRequest;
use DefStudio\Telegraph\Models\TelegraphBot;
use Illuminate\Http\Request;

class ConfigController extends Controller
{
    public function create(CreateBotRequest $request)
    {
        $bot = TelegraphBot::create([
            'token' => $request->token,
            'name' => $request->name,
        ]);
        dd($bot->registerWebhook()->send());
    }
}
