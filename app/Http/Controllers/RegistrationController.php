<?php

namespace App\Http\Controllers;


use App\Hendlers\CustomWebhookHandler;
use App\Models\BotUpdate;
use App\Models\ExamDay;
use App\Models\ExamDayItem;
use App\Models\MyBot;
use App\Models\User;
use App\Models\UserRegistration;
use App\Services\TelegramService;
use DefStudio\Telegraph\Keyboard\Button;
use DefStudio\Telegraph\Models\TelegraphBot;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;


class RegistrationController extends Controller
{
    public function index(Request $request)
    {
//        User::query()->where('id',27223)->delete();
//        ExamDay::where('id',1)->update(['date' => '2023-07-26']);
        $bot = MyBot::query()->first();
        $botUpdate = new BotUpdate($request);
        app(CustomWebhookHandler::class)->handle($request,$bot);

        return true;
    }

    public function registerWebhook()
    {
        return TelegramService::getInstance()->registerWebhook();
    }

    public function unRegisterWebhook()
    {
        return TelegramService::getInstance()->unRegisterWebhook();
    }

    public function create(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'name' => ['required', 'string', 'max:255'],
            'token' => ['required', 'string', 'max:255']
        ]);
        if($validation->fails())
            return \response()->json($validation->errors()->messages());
        TelegraphBot::create($request->all());
        return  \response()->json(true);
    }

}
