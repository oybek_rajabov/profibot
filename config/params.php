<?php

use Illuminate\Support\Facades\Facade;
use Illuminate\Support\ServiceProvider;

return [
    'upload_file_server' => 'https://admin.profiuniversity.uz/api/upload-file',
    'get_file_server' => 'https://admin.profiuniversity.uz/',
];
